<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
    
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ScoreCart-HomePage</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
   <div class="container">
  <h2>SCORECART</h2>
  <br>
  <!-- Nav pills -->
  <ul class="nav nav-pills" >
    
    <li class="nav-item">
  
      <a class="nav-link" data-toggle="pill" data-target="#menu1" href="<%=request.getContextPath()%>/list">CATEGORY</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="pill" href="#menu2">LEADERBOARD</a>
    </li>
  </ul>  

  <!-- Tab panes -->
  <div class="tab-content">
    
    <div id="menu1" class="container tab-pane fade"><br>   	
			
	
			<div class="container text-left">
				<a href="<%=request.getContextPath()%>/new" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add
					New Category</a>
			</div>
			<br>
			<div class="container">
			  <h3 class="text-center">List of Category</h3>
			<table class="table table-bordered">
				<thead>
					<tr>						
						<th>Title</th>
						<th>Description</th>
						<th>Actions</th>						
					</tr>
				</thead> 
				<tbody>				
					<c:forEach var="cat" items="${listcat}">
											<tr>							
							<td><c:out value='${cat.CategoryTitle}'/></td>
							<td><c:out value='${cat.CategoryDesc}'/></td>
							
							<td><a href="edit?id=<c:out value='${cat.Id}'/>" data-toggle="modal" data-target="#myModal">Edit</a>
								&nbsp;&nbsp;&nbsp;&nbsp; <a
								href="delete?id=<c:out value='${cat.Id}'/>">Delete</a></td>
						</tr>
					</c:forEach>
		
				</tbody>

			</table>
		</div>
		
<div class="container">
		<div class="modal" id="myModal">
       <div class="modal-dialog">
       <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
        <c:if test="${user != null}">
            			Edit Category
            		</c:if>
						<c:if test="${user == null}">
            			Add  Category
            		</c:if>
            		</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <c:if test="${user != null}">
					<form action="update" method="post">
				</c:if>
				<c:if test="${user == null}">
					<form action="insert" method="post">
				</c:if>
				<c:if test="${user != null}">
					<input type="hidden" name="id" value="<c:out value='${user.id}' />" />
				</c:if>
				

				<fieldset class="form-group">
					<label>Category Name</label>
					 <input type="text" name="title" class="form-control" required="required"
						value="<c:out value='${user.title}'/>" 
						 />
				</fieldset>

				<fieldset class="form-group">
					<label>Description</label> <input type="text" name="Desc" class="form-control"
						value="<c:out value='${user.Desc}'/>" 
						/>
				</fieldset>

				
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
</div>
	
    
    </div>
    <div id="menu2" class="container tab-pane fade"><br>
      <h3>LEADERBOARD</h3>
      
    </div>
  </div>
</div>
</body>
</html>