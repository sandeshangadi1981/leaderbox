package com.ScoreLeaderBox.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ScoreLeaderBox.bean.Category;
 

public class CategoryDao {
	
	private String jdbcURL;
    private String jdbcUsername;
    private String jdbcPassword;
    private Connection jdbcConnection;
     
    public CategoryDao(String jdbcURL, String jdbcUsername, String jdbcPassword) {
        this.jdbcURL = jdbcURL;
        this.jdbcUsername = jdbcUsername;
        this.jdbcPassword = jdbcPassword;
    }
     
    protected void connect() throws SQLException {
        if (jdbcConnection == null || jdbcConnection.isClosed()) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                throw new SQLException(e);
            }
            jdbcConnection = DriverManager.getConnection(
                                        jdbcURL, jdbcUsername, jdbcPassword);
        }
    }
     
    protected void disconnect() throws SQLException {
        if (jdbcConnection != null && !jdbcConnection.isClosed()) {
            jdbcConnection.close();
        }
    }
    
    public boolean insertCategory(Category category) throws SQLException {
        String sql = "INSERT INTO category (Category_title, Category_desc) VALUES (?, ?)";
        connect();
         
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setString(1, category.getCategoryTitle());
        statement.setString(2, category.getCategoryDesc());
       
         
        boolean rowInserted = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowInserted;
    }
    
    public List<Category> listAllCategory() throws SQLException {
        List<Category> listCategory = new ArrayList<>();
         
        String sql = "SELECT * FROM category";
         
        connect();
         
        Statement statement = jdbcConnection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
         
        while (resultSet.next()) {
            int id = resultSet.getInt("Category_id");
            String title = resultSet.getString("Category_title");
            String desc = resultSet.getString("Category_desc");
            
             
            Category cat = new Category(id, title, desc);
            listCategory.add(cat);
        }
         
        resultSet.close();
        statement.close();
         
        disconnect();
         
        return listCategory;
    }
    
    public boolean deleteCategory(Category category) throws SQLException {
        String sql = "DELETE FROM category where Category_id = ?";
         
        connect();
         
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setInt(1, category.getId());
         
        boolean rowDeleted = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowDeleted;     
    }
    
    public boolean updateCategory(Category category) throws SQLException {
        String sql = "UPDATE category SET Category_title = ?, Category_desc = ?";
        sql += " WHERE Category_id = ?";
        connect();
         
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setString(1, category.getCategoryTitle());
        statement.setString(2, category.getCategoryDesc());    
        statement.setInt(3, category.getId());
         
        boolean rowUpdated = statement.executeUpdate() > 0;
        statement.close();
        disconnect();
        return rowUpdated;     
    }
     
    public Category getCategory(int id) throws SQLException {
    	Category cat = null;
        String sql = "SELECT * FROM category WHERE Category_id = ?";
         
        connect();
         
        PreparedStatement statement = jdbcConnection.prepareStatement(sql);
        statement.setInt(1, id);
         
        ResultSet resultSet = statement.executeQuery();
         
        if (resultSet.next()) {
            String title = resultSet.getString("Category_title");
            String desc = resultSet.getString("Category_desc");
            
             
            cat = new Category(id, title, desc);
        }
         
        resultSet.close();
        statement.close();
         
        return cat;
    }

}
