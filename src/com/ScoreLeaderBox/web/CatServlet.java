package com.ScoreLeaderBox.web;




import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ScoreLeaderBox.bean.Category;
import com.ScoreLeaderBox.dao.CategoryDao;
import com.google.gson.Gson;

import java.sql.SQLException;
import java.util.List;
 
import javax.servlet.RequestDispatcher;





/**
 * Servlet implementation class CatServlet
 */
@WebServlet("/CatServlet")
public class CatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CategoryDao catDAO;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CatServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() {
        String jdbcURL = getServletContext().getInitParameter("jdbcURL");
        String jdbcUsername = getServletContext().getInitParameter("jdbcUsername");
        String jdbcPassword = getServletContext().getInitParameter("jdbcPassword"); 
        catDAO = new CategoryDao(jdbcURL, jdbcUsername, jdbcPassword);
 
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {    	
        doGet(request, response);
    }

	

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath(); 
       // String catLoad = request.getParameter("action");
        
        //if (catLoad.equals("loadCat"))
		// {
        //	 action = "/list";
		// }
 
        try {
            switch (action) {
            case "/new":
                showNewForm(request, response);
                break;
            case "/insert":
                insertCategory(request, response);
                break;
            case "/delete":
                deleteCategory(request, response);
                break;
            case "/edit":
                showEditForm(request, response);
                break;
            case "/CatServlet":
                showEditForm(request, response);
                break;
            case "/update":
                updateCategory(request, response);
                break;
            default:
                listCategory(request, response);
                break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }
    
    private void listCategory(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<Category> listcat = catDAO.listAllCategory();
        request.setAttribute("listcat", listcat);
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }
 
    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }
 
    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
    	response.setContentType("application/json");
        int id = Integer.parseInt(request.getParameter("id"));
        Category existingcategory = catDAO.getCategory(id);
        Gson gson = new Gson();
        PrintWriter writer = response.getWriter();
        writer.print(gson.toJson(existingcategory));
        writer.flush();
        writer.close();
        //RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
       // request.setAttribute("category", existingcategory);
       // dispatcher.forward(request, response);
 
    }
 
    private void insertCategory(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String title = request.getParameter("categoryName");
        String desc = request.getParameter("description");    
 
        Category newcat = new Category(title, desc);
        catDAO.insertCategory(newcat);
        response.sendRedirect("list");
    }
 
    private void updateCategory(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("categoryTitle");
        String desc = request.getParameter("categoryDesc");       
 
        Category newcat = new Category(id,title, desc);
        catDAO.updateCategory(newcat);
        response.sendRedirect("list");
    }
 
    private void deleteCategory(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id")); 
        Category cat = new Category(id);
        catDAO.deleteCategory(cat);
        response.sendRedirect("list");
 
    }
	

}
