package com.ScoreLeaderBox.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.ScoreLeaderBox.bean.Category;
import com.ScoreLeaderBox.bean.Score;
import com.ScoreLeaderBox.dao.CategoryDao;
import com.ScoreLeaderBox.dao.ScoreDao;
import com.google.gson.Gson;

import java.sql.SQLException;
import java.util.List;
 
import javax.servlet.RequestDispatcher;

/**
 * Servlet implementation class ScoreServlet
 */
@WebServlet("/ScoreServlet")
public class ScoreServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CategoryDao catDAO;
	private ScoreDao scoreDAO; 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ScoreServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		String jdbcURL = "jdbc:mysql://localhost:3306/mycart";
        String jdbcUsername = "root";
        String jdbcPassword = "Nov@2020"; 
        catDAO = new CategoryDao(jdbcURL, jdbcUsername, jdbcPassword);
        scoreDAO = new ScoreDao(jdbcURL, jdbcUsername, jdbcPassword);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws  ServletException, IOException {
		// TODO Auto-generated method stub
		 try {
			 String action = request.getParameter("action");			 
			 if (action.equals("score"))
			 {
				 listScore(request, response);
			 }
			 else
			 {
				 listCategory(request, response);
			 }
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
	}
	
	private void listScore(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
		response.setContentType("application/json");
		 int id = Integer.parseInt(request.getParameter("id"));
        List<Score> listScore = scoreDAO.listAllScore(id);
        Gson gson = new Gson();       
        PrintWriter writer = response.getWriter();
        writer.print(gson.toJson(listScore));
        writer.flush();
        writer.close();
    }
	
	private void listCategory(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
		response.setContentType("application/json");
        List<Category> listcat = catDAO.listAllCategory();
        Gson gson = new Gson();       
        PrintWriter writer = response.getWriter();
        writer.print(gson.toJson(listcat));
        writer.flush();
        writer.close();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			insertScore(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	 private void insertScore(HttpServletRequest request, HttpServletResponse response)
	            throws SQLException, IOException, ServletException {
		 
		 String category = request.getParameter("category");
		 String scorenum = request.getParameter("score");
		 int catId = Integer.valueOf(category);
	     int scoreNo = Integer.valueOf(scorenum);
	     String name = request.getParameter("name");  
	     Score score = new Score(catId, scoreNo, name);
	     scoreDAO.insertScore(score);
	     RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
	     dispatcher.forward(request, response);
	    }

}
