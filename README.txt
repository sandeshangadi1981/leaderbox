
Steps to run the Project as follows:

Prerequisites:
Java(jdk-15.0.1)
Maven 3.6.3
Tomcat9
Git
Mysql8.0



Run following scripts in MySQL server.

Login to mySQl server.

/*
SQLyog Community
MySQL - 8.0.21 : Database - mycart
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mycart` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `mycart`;


/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `Category_id` int NOT NULL AUTO_INCREMENT,
  `Category_title` varchar(128) NOT NULL,
  `Category_desc` varchar(45) NOT NULL,
  PRIMARY KEY (`Category_id`),
  UNIQUE KEY `Categoryid_UNIQUE` (`Category_id`),
  UNIQUE KEY `title_UNIQUE` (`Category_title`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;

/*Table structure for table `score` */

DROP TABLE IF EXISTS `score`;

CREATE TABLE `score` (
  `score_id` int NOT NULL AUTO_INCREMENT,
  `Cat_id` int NOT NULL,
  `name` varchar(128) NOT NULL,
  `score` int DEFAULT NULL,
  PRIMARY KEY (`score_id`),
  UNIQUE KEY `score_id_UNIQUE` (`score_id`),
  KEY `par_ind` (`Cat_id`),
  CONSTRAINT `score_ibfk_1` FOREIGN KEY (`Cat_id`) REFERENCES `category` (`Category_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;



Clone URL : 
   
git clone https://gitlab.com/sandeshangadi1981/leaderbox.git



Go to your downloaded project folder and run the below command:

mvn clean install

on successful build war file will be generated under target folder.

copy war file and paste it in Tomcat webapps folder.

Now run the tomcat startup.bat(location will be under tomcat bin folder)

open the browser and paste the below URL

http://localhost:8080/ScoreLeaderBox-0.0.1-SNAPSHOT/




