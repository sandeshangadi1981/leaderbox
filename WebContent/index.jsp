<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ScoreCart-HomePage</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
	integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>

<style>
.delete {
	text-align: center;
}

.edit {
	text-align: center;
}
.leaderbox {
	text-align: center;
}
.catText {
	text-align: center;
}
</style>

<script>
	$(document).ready(function() {
		var catname;	
		var id ;
		/* loadCategory();
		function loadCategory()
		{
			$.ajax({
				type:'GET',
				url:'CatServlet',
				data:{action:'loadCat'},				
				success:function(){					
					alert("categoryloaded");
					
				}
			});
		} */
		
		
		$('table .delete').on('click', function(){			
			var id =$(this).parent().find('#id').val();			
			$('#myDeleteModal #id').val(id);			
		});
		
		$('table .leaderbox').on('click', function(){			
			catname =$(this).text();	
			id =$(this).parent().find('#catid').val();
			 alert(catname);
			 alert(id);
			$('.nav-pills a[href="#menu2"]').tab('show')			
		});
		
		$('.nav-pills a[href="#menu2"]').on('shown.bs.tab', function(e){ 
			$('span.catText').text(catname);
			$.ajax({
				type:'GET',
				url:'ScoreServlet',
				data:{action:'score',id:id},
				datatype:'json',
				contenttype:'application/json',
				success:function(result){					
					var json = JSON.parse(result);
                    $.each(json, function (index, obj) { 
                        var row = '<tr><td> ' + obj.name + ' </td> <td> ' + obj.scoreNo +  '</td> </tr>'
                        $("#scoreList tbody").append(row);
                    }); 
					
				}
			});
	    })
		
		$('table .edit').on('click', function(){			
			var id =$(this).parent().find('#id').val();				
			$.ajax({
				type:'GET',
				url:'CatServlet',
				data:{action:'find',id:id},
				datatype:'json',
				contenttype:'application/json',
				success:function(result){	
					$('#myUpModal #id').val(result.id);
					$('#myUpModal #categoryTitle').val(result.categoryTitle);
					$('#myUpModal #categoryDesc').val(result.categoryDesc);
					
				}
			});
			
		});
		
		$('#reset').on('click', function(){							
			$('#name').val("");
			$('#score').val("");			
			 $('#category').prop('selectedIndex',0);
		});
		
		$('#score').on('click', function(){				
			$.ajax({
				type:'GET',
				url:'ScoreServlet',				
				datatype:'json',
				contenttype:'application/json',
				success:function(result){					
					for(var i=0;i<result.length;i++)
					{
					$('#category').append($('<option>',
							{
							value : result[i].id,
							text : result[i].categoryTitle								
							}
					
					));
					
					}	
					
				}
			});
		});
	});
</script>
</head>
<body>
	<div class="container">
		<h2>SCORECART</h2>
		<br>
		<!-- Nav pills -->
		<ul class="nav nav-pills">
		

			<li class="nav-item"><a class="nav-link active " data-toggle="pill" data-target="#menu1" 				
				href="<%=request.getContextPath()%>/list">CATEGORY</a></li>
				<li class="nav-item">
      <a class="nav-link " id="score" data-toggle="pill" href="<%=request.getContextPath()%>/score" data-target="#home" >SCORE</a>
        </li>
			<li class="nav-item"><a class="nav-link" data-toggle="pill"
				href="#menu2" data-target="">LEADERBOARD</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">

			<div id="menu1" class="container tab-pane active">
				<br>


				<div class="container text-left">
					<a href="#myInModal" class="btn btn-success" data-toggle="modal" >Add New Category</a>						
				</div>
				<br>
				<div class="container">
					<h3 class="text-center">List of Category</h3>
					<table id="categoryList" class="table table-bordered">
						<thead>
							<tr>
								<th>Title</th>
								<th>Description</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="cat" items="${listcat}">
								<tr>
									<td><input type="hidden" name="catid" id="catid" value="${cat.id}"/><a id="catTitle" class="leaderbox"><c:out value='${cat.categoryTitle}' /></a></td>
									<td><c:out value='${cat.categoryDesc}' /></td>
									<td>
									<a class="edit" data-toggle="modal" data-target="#myUpModal">Edit</a>
										 &nbsp;&nbsp;&nbsp;&nbsp; 
									<a class="delete" data-toggle="modal" data-target="#myDeleteModal">Delete</a> 
									<input type="hidden" name="id" id="id" value="${cat.id}"/>		
									</td>
								</tr>
							</c:forEach>

						</tbody>

					</table>
				</div>

				<div class="container">
					<div class="modal" id="myUpModal">
						<div class="modal-dialog">
							<div class="modal-content">
								<!-- Modal Header -->
								<div class="modal-header">
									<h4 class="modal-title">Edit Category</h4>
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>

								<!-- Modal body -->
								<div class="modal-body">
									<form action="update" method="post">
										<input type="hidden" name="id" id="id" />
										<fieldset class="form-group">
											<label>CategoryName</label>
											 <input type="text" name="categoryTitle" class="form-control" required="required" id="categoryTitle"  />												
										</fieldset>
										<fieldset class="form-group">
											<label>Description</label>
											 <input type="text" name="categoryDesc" class="form-control" id="categoryDesc"  />											
										</fieldset>
										<!-- Modal footer -->
										<div class="modal-footer">
											<button type="submit" class="btn btn-success">Save</button>
											<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>												
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>

					<div class="modal" id="myInModal">
						<div class="modal-dialog">
							<div class="modal-content">

								<!-- Modal Header -->
								<div class="modal-header">
									<h4 class="modal-title">Add Category</h4>
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>

								<!-- Modal body -->
								<div class="modal-body">
									<form action="insert" method="post">
										<fieldset class="form-group">
											<label>CategoryName</label> 
											<input type="text" name="categoryName" class="form-control" required="required" id="categoryTitlei" />
										</fieldset>
										<fieldset class="form-group">
											<label>Description</label>
											 <input type="text" name="description" class="form-control" id="categoryDesci"  />												
										</fieldset>
										<!-- Modal footer -->
										<div class="modal-footer">
											<button type="submit" class="btn btn-success">Save</button>
											<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>												
										</div>
									</form>
								</div>

							</div>
						</div>
					</div>
				</div>






				<div class="container">
					<div class="modal" id="myDeleteModal">
						<div class="modal-dialog">
							<div class="modal-content">
								<form method="post" action="delete">
									<!-- Modal Header -->
									<div class="modal-header">
										<h4 class="modal-title">Delete Category</h4>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>

									<!-- Modal body -->
									<div class="modal-body">
										<p>Are you sure you want to delete Record?</p>
										<p class="text-warning">
											<small>This action cannot be undone.</small>
										</p>
									</div>

									<!-- Modal footer -->
									<div class="modal-footer">
										<button type="submit" class="btn btn-danger" value="delete">Delete</button>
										<button type="button" class="btn btn-default" data-dismiss="modal" value="cancel">Cancel</button>											
										<input type="hidden" name="id" id="id">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>


			</div>
			<div id="home" class="container tab-pane fade "><br>
              <h2>ADDSCORE</h2> 
              <div class="container">
									
				<form action="score" method="post">
										
										<div class="form-group">
											<label>Name</label>
											 <input type="text" style="width:250px" name="name" class="form-control" required="required" id="name" placeholder="Please Enter Name"  />												
										</div>
										<div class="form-group">
											<label>Score</label>
											 <input type="text" style="width:250px" name="score" class="form-control" required="required" id="score" placeholder="Please Enter Name"  />											
										</div>
										<div class="form-group">
											<label>Category</label>	
											<select id="category" name="category" required="required" class="form-control" style="width:250px">
											<option value="0">Select Category</option>
											
											</select>																				
										</div>
										 <div class="container">
											<button type="submit" class="btn btn-success">Save</button>
											<button type="button" class="btn btn-default" id="reset">Reset</button>	
										</div>											
										
				</form>
								
						</div>    
             </div>
			<div id="menu2" class="container tab-pane fade">
				<div class="container">
					<h3 class="text-center">List of Score for <span class="catText"></span> Category</h3>
					<table id="scoreList" class="table table-bordered">
						<thead>
							<tr>
								<th>Name</th>
								<th>Score</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>

					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>